﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public interface IFileRepositorio
    {
        T[] getFile<T>(string ruta, bool ignoreError = false) where T : class;
        T[] getFile<T>(FileHelperAsyncEngine<T> engine, int maxRecords, int skipRecords) where T : class;
        FileHelperAsyncEngine<T> getEngine<T>(string ruta, bool ignoreError = true) where T : class;
        T[] getFile<T>(Stream stream, bool ignoreError = true) where T : class;
        DataTable getFileDataTable<T>(string ruta, bool ignoreError = true) where T : class;
        void writeFile<T>(string name, List<T> objeto) where T : class;
        void appendFile<T>(string name, T objeto) where T : class;
        void appendFile<T>(string name, List<T> objeto) where T : class;
    }
}
