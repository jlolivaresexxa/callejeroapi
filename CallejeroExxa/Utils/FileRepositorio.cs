﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class FileRepositorio : IFileRepositorio
    {
        public T[] getFile<T>(string ruta, bool ignoreError = false) where T : class
        {
            var engine = new FileHelperEngine<T>();
            if (ignoreError)
                engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            T[] records = engine.ReadFile(ruta);
            //ExceptionlessClient.Default.Configuration.SetUserIdentity("aaa@aa.aa", "prueba");
            //engine.ErrorManager.Errors.ToList().ForEach(U=>               
            //    ExceptionlessClient.Default.SubmitLog("linea:"+U.LineNumber+";contenido:"+ U.RecordString + ";error:" +U.ExceptionInfo.Message));    
            return records;
        }
        public DataTable getFileDataTable<T>(string ruta, bool ignoreError = true) where T : class
        {
            var engine = new FileHelperEngine<T>();
            if (ignoreError)
                engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            DataTable records = engine.ReadFile(ruta).ToDataTable<T>();
            return records;
        }
        public T[] getFile<T>(Stream stream, bool ignoreError = true) where T : class
        {
            var engine = new FileHelperEngine<T>();
            if (ignoreError)
                engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;
            stream.Seek(0, SeekOrigin.Begin);

            T[] records = engine.ReadStream(new StreamReader(stream), 100);
 
            return records;
        }

        public T[] getFile<T>(FileHelperAsyncEngine<T> engine, int maxRecords, int skipRecords) where T : class
        {

            T[] records = engine.ReadNexts(maxRecords);
            return records;
        }

        public void writeFile<T>(string name, List<T> objeto) where T : class
        {
            var engine = new FileHelperEngine<T>();
            //Creamos el directorio si no existe
            Directory.CreateDirectory(Path.GetDirectoryName(name));
            engine.WriteFile(name, objeto);
        }

        public Stream writeStream<T>(List<T> objeto, bool conCabecera = false) where T : class
        {
            MemoryStream stream = new MemoryStream();
            StringBuilder sb = new StringBuilder();
            StreamWriter writer = new StreamWriter(stream);
            var engine = new FileHelperEngine<T>();

            //con esta propiedad a true escribe todas las líneas del fichero
            writer.AutoFlush = true;

            if (conCabecera)
                engine.HeaderText = engine.GetFileHeader();

            engine.WriteStream(writer, objeto);

            return stream;
        }

        public void appendFile<T>(string name, T objeto) where T : class
        {
            var engine = new FileHelperEngine<T>();
            engine.AppendToFile(name, objeto);
        }
        public void appendFile<T>(string name, List<T> objeto) where T : class
        {
            var engine = new FileHelperEngine<T>();
            engine.AppendToFile(name, objeto);
        }

        public FileHelperAsyncEngine<T> getEngine<T>(string ruta, bool ignoreError = true) where T : class
        {
            var engine = new FileHelperAsyncEngine<T>();
            if (ignoreError)
                engine.ErrorManager.ErrorMode = ErrorMode.SaveAndContinue;

            engine.BeginReadFile(ruta);
            return engine;
        }


    }

}
