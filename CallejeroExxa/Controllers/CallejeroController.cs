﻿using CallejeroExxa.Models.DTO;
using CallejeroExxa.Models.Objetos;
using CallejeroGEOAPI;
using CallejeroGEOAPI.ObjetosAPI;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Utils;

namespace CallejeroExxa.Controllers
{
    public class CallejeroController : Controller
    {
        private string rutaFichero = @"C:\IO\direccionesSuministro.csv";
        private IDictionary<string, Provincia> listadoProvinciasCallejero;
        private IDictionary<string, Municipio> listadoMunicipiosCallejero;
        private IDictionary<string, Poblacion> listadoPoblacionesCallejero;
        private IDictionary<string, CodigoPostal> listadoCodigosPostalesCallejero;
        private IDictionary<string, Via> listadoViasCallejero;
        private ConsultasGEOAPI consultasAPI;
        private List<string> abreviaCalles = new List<string>()
        {
            "CALLE",
            "Calle",
            "CL",
            "Cl",
            "C/",
            "AV",
            "AVENIDA",
            "Avenida",
            "PLAZA",
            "Plaza",
            "PL",
            "Pl",
            "PASEO",
            "Paseo",
            "CAMINO",
            "Camino",
            "URB",
            "URBANIZACIÓN",
            "URBANIZACION",
            "Urb",
            "Urbanización",
            "Urbanizacion"
        };


        // GET: Callejero
        public ActionResult Index()
        {
            return View();
        }

        #region Métodos públicos
        [HttpGet]
        public JsonResult GetListaDireccionesDesdeFichero()
        {
            List<FicheroDireccionesCallejeroDTO> listaDireccionesCallejero = new List<FicheroDireccionesCallejeroDTO>();
            //Consultas a la API
            consultasAPI = new ConsultasGEOAPI();

            //Devuelve el listado completo de provincias de la API
            GetListadoProvincias();

            //Recogemos la lista de direcciones a consultar 
            List<FicheroDireccionesDTO> listaDirecciones = GetListaDireccionesDesdeFichero(rutaFichero);
            foreach(FicheroDireccionesDTO direccion in listaDirecciones)
            {
                FicheroDireccionesCallejeroDTO direccionCallejero = GetDireccionCallejero(direccion);
                if (direccionCallejero != null)
                    listaDireccionesCallejero.Add(direccionCallejero);
            }

            string rutaFicheroDestino = @"C:\IO\direccionesCallejero.csv";
            this.SetListaDireccionesToFichero(rutaFicheroDestino, listaDireccionesCallejero);

            return Json(new { estado = "ok" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Métodos Privados

        private FicheroDireccionesCallejeroDTO GetDireccionCallejero(FicheroDireccionesDTO direccionFichero)
        {
            FicheroDireccionesCallejeroDTO direccionCallejero = new FicheroDireccionesCallejeroDTO();
            string nombreVia = string.Empty;
            bool busquedaFallida = false;

            direccionCallejero = this.getNombreViaDireccionFichero(direccionFichero.domicilio);
            direccionCallejero.calleCompleta = direccionFichero.domicilio;
            direccionCallejero.id = direccionFichero.id;
            direccionCallejero.nombreMunicipio = direccionFichero.nombreMunicipio.ToUpper();
            direccionCallejero.nombreProvincia = direccionFichero.nombreProvincia.ToUpper();
            direccionCallejero.codigoPostal = direccionFichero.codigoPostal;

            List<Via> calles = this.GetListadoCallesPorConsultaNombre(direccionCallejero.calle);
            if (calles.Count > 0)
            { 
                Via calle = calles.Where(x => x.CodigoPostal == direccionFichero.codigoPostal).FirstOrDefault();
                if (calle != null)
                {
                    direccionCallejero.calle = calle.NombreVia.ToUpper();
                    direccionCallejero.codigoPostal = calle.CodigoPostal;
                }
                else
                    busquedaFallida = true;
            }
            else
                busquedaFallida = true;


            if (busquedaFallida)
            {
                string codigoProvincia = listadoProvinciasCallejero.Where(x => x.Value.NombreProvincia.ToUpper().Contains(direccionFichero.nombreProvincia.ToUpper())).FirstOrDefault().Key;
                this.GetListadoMunicipiosPorProvincia(codigoProvincia);

                var provincia = listadoProvinciasCallejero.Where(x => x.Key == codigoProvincia).FirstOrDefault();
                if (provincia.Value != null)
                {
                    var municipio =listadoMunicipiosCallejero.Where(mun => mun.Value.CodigoProvincia == provincia.Key && mun.Value.NombreMunicipio.ToUpper().Contains(direccionFichero.nombreMunicipio.ToUpper())).FirstOrDefault();
                    if (municipio.Value != null)
                    {
                        this.GetListadoPoblacionesPorMunicipio(codigoProvincia, municipio.Value.CodigoMunicipio);
                        if(listadoPoblacionesCallejero != null)
                        {
                            var poblacion = listadoPoblacionesCallejero.Where(pob => pob.Value.NombrePoblacion.ToUpper().Contains(direccionFichero.nombreMunicipio.ToUpper())).FirstOrDefault();
                            if (poblacion.Value != null)
                                this.GetListadoCallesPorCodigoPostal(codigoProvincia, municipio.Key, poblacion.Key, direccionFichero.codigoPostal);

                            if(listadoViasCallejero != null)
                            {
                                //Buscamos las vías del código postal pasado por fichero.
                                var via = listadoViasCallejero.Where(x => x.Value.NombreVia.Contains(direccionCallejero.calle) || direccionCallejero.calle.Contains(x.Value.NombreVia)).FirstOrDefault();

                                if (via.Value != null)
                                {
                                    direccionCallejero.calle = via.Value.NombreVia.ToUpper();
                                    direccionCallejero.codigoPostal = via.Value.CodigoPostal;
                                }
                            }
                        }
                    }
                }
            }

            return direccionCallejero;
        }

        private List<FicheroDireccionesDTO> GetListaDireccionesDesdeFichero(string rutaFichero)
        {
            List<FicheroDireccionesDTO> listaDirecciones = new List<FicheroDireccionesDTO>();
            try
            {
                FileRepositorio fileRepo = new FileRepositorio();
                listaDirecciones = fileRepo.getFile<FicheroDireccionesDTO>(this.rutaFichero).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en GetFicheroDirecciones: " + ex.Message);
            }
            return listaDirecciones;
        }
        private void SetListaDireccionesToFichero(string rutaFichero, List<FicheroDireccionesCallejeroDTO> datos)
        {

            try
            {
                FileRepositorio fileRepo = new FileRepositorio();
                fileRepo.writeFile(rutaFichero, datos);
            }
            catch (Exception ex)
            {
                throw new Exception("Error en SetListaDireccionesToFichero: " + ex.Message);
            }

        }

        #region GET INFO DEL CALLEJERO
        private void GetListadoProvincias()
        {
            string JSONProvincias = consultasAPI.consultaProvincias();
            JSONProvincias = JObject.Parse(JSONProvincias).SelectToken("data").ToString();

            List<ProvinciaGEOAPI> provinciaAPI = JsonConvert.DeserializeObject<List<ProvinciaGEOAPI>>(JSONProvincias);
            if(provinciaAPI != null)
            {
                foreach(ProvinciaGEOAPI provincia in provinciaAPI)
                {
                    Provincia provinciaCallejero = new Provincia()
                    {
                        CodigoComunidad = provincia.CCOM,
                         CodigoProvincia = provincia.CPRO,
                          NombreProvincia = provincia.PRO
                    };
                    if (listadoProvinciasCallejero == null)
                        listadoProvinciasCallejero = new Dictionary<string, Provincia>();

                    if (!listadoProvinciasCallejero.ContainsKey(provincia.CPRO))
                    {
                        listadoProvinciasCallejero.Add(provincia.CPRO, provinciaCallejero);
                    }
                        
                }
            }
        }
        private void GetListadoMunicipiosPorProvincia(string codigoProvincia)
        {
            string JSONMunicipiosAPI = consultasAPI.consultaMunicipiosPorProvincia(codigoProvincia);
            JSONMunicipiosAPI = JObject.Parse(JSONMunicipiosAPI).SelectToken("data").ToString();

            List<MunicipioGEOAPI> municipiosAPI = JsonConvert.DeserializeObject<List<MunicipioGEOAPI>>(JSONMunicipiosAPI);
            if (municipiosAPI != null)
            {
                foreach (MunicipioGEOAPI municipioAPI in municipiosAPI)
                {
                    Municipio municipio = new Municipio()
                    {
                        CodigoMunicipio = municipioAPI.CMUM,
                        CodigoProvincia = municipioAPI.CPRO,
                        NombreMunicipio = municipioAPI.DMUN50
                    };
                    this.AñadeMunicipio(codigoProvincia, municipio);
                }
            }
        }
        private void GetListadoPoblacionesPorMunicipio(string codigoProvincia, string codigoMunicipio)
        {
            string JSONPoblacionesAPI = consultasAPI.consultaPoblacionesPorMunicipio(codigoProvincia, codigoMunicipio);
            JSONPoblacionesAPI = JObject.Parse(JSONPoblacionesAPI).SelectToken("data").ToString();

            List<PoblacionGEOAPI> poblacionesAPI = JsonConvert.DeserializeObject<List<PoblacionGEOAPI>>(JSONPoblacionesAPI);
            if (poblacionesAPI != null)
            {
                foreach (PoblacionGEOAPI municipioAPI in poblacionesAPI)
                {
                    Poblacion poblacion = new Poblacion()
                    {
                        CodigoMunicipio = municipioAPI.CMUM,
                        CodigoProvincia = municipioAPI.CPRO,
                        CodigoPoblacion = municipioAPI.CUN,
                        NombrePoblacion = municipioAPI.NENTSI50
                    };
                    this.AñadePoblacion(codigoProvincia, codigoMunicipio, poblacion);
                }
            }
        }
        private void GetListadoCodigosPostalesPorPoblacion(string codigoProvincia, string codigoMunicipio, string codigoPoblacion)
        {
            string JSONCodigosPostalesAPI = consultasAPI.consultaCodigosPostalesPorPoblacion(codigoProvincia, codigoMunicipio, codigoPoblacion);
            JSONCodigosPostalesAPI = JObject.Parse(JSONCodigosPostalesAPI).SelectToken("data").ToString();

            List<CodigoPostalGEOAPI> CodigosPostalesAPI = JsonConvert.DeserializeObject<List<CodigoPostalGEOAPI>>(JSONCodigosPostalesAPI);
            if (CodigosPostalesAPI != null)
            {
                foreach (CodigoPostalGEOAPI codigoPostalAPI in CodigosPostalesAPI)
                {
                    CodigoPostal cp = new CodigoPostal()
                    {
                        CodigoMunicipio = codigoPostalAPI.CMUM,
                        CodigoProvincia = codigoPostalAPI.CPRO,
                        CodigoPoblacion = codigoPostalAPI.CUN,
                        CP = codigoPostalAPI.CPOS
                    };
                    this.AñadeCodigoPostal(codigoProvincia, codigoMunicipio, codigoPoblacion, cp);
                }
            }
        }
        private void GetListadoCallesPorCodigoPostal(string codigoProvincia, string codigoMunicipio, string codigoPoblacion, string codigoPostal)
        {
            string JSONCallejeroAPI = consultasAPI.consultaCallejeroPostal(codigoProvincia, codigoMunicipio, codigoPoblacion, codigoPostal);
            JSONCallejeroAPI = JObject.Parse(JSONCallejeroAPI).SelectToken("data").ToString();

            List<CalleGEOAPI> callesGEOAPI = JsonConvert.DeserializeObject<List<CalleGEOAPI>>(JSONCallejeroAPI);
            if (callesGEOAPI != null)
            {
                foreach (CalleGEOAPI calleAPI in callesGEOAPI)
                {
                    Via calle = new Via()
                    {
                        CodigoMunicipio = calleAPI.CMUM,
                        CodigoProvincia = calleAPI.CPRO,
                        CodigoPoblacion = calleAPI.CUN,
                        CodigoPostal = calleAPI.CPOS,
                        CodigoVia = calleAPI.CVIA,
                        TipoVia = calleAPI.TVIA,
                        NombreVia = calleAPI.NVIAC
                    };
                    this.AñadeViaACodigoPostal(codigoProvincia, codigoMunicipio, codigoPoblacion, codigoPostal, calle);
                }
            }
        }
        private List<Via> GetListadoCallesPorConsultaNombre(string nombreCalle)
        {
            List<Via> listaVias = new List<Via>();
            string JSONCalles = consultasAPI.consultaCallejeroPorTexto(nombreCalle);
            JSONCalles = JObject.Parse(JSONCalles).SelectToken("data").ToString();

            List<CalleGEOAPI> callesAPI = JsonConvert.DeserializeObject<List<CalleGEOAPI>>(JSONCalles);
            callesAPI.ForEach(via =>
            {
                Via calle = new Via()
                {
                    NombreVia = via.NVIAC,
                     TipoVia = via.TVIA,
                      CodigoMunicipio = via.CMUM,
                       CodigoVia = via.CVIA,
                        CodigoPostal = via.CPOS,
                         CodigoPoblacion = via.CUN,
                          CodigoProvincia = via.CPRO
                };
                listaVias.Add(calle);
            });

            return listaVias;
        }

        private void AñadeMunicipio(string codigoProvincia, Municipio municipio)
        {
            if (listadoMunicipiosCallejero == null)
                listadoMunicipiosCallejero = new Dictionary<string, Municipio>();

            if (!listadoMunicipiosCallejero.ContainsKey(municipio.CodigoMunicipio))
                listadoMunicipiosCallejero.Add(municipio.CodigoMunicipio, municipio);
        }
        private void AñadePoblacion(string codigoProvincia, string codigoMunicipio, Poblacion poblacion)
        {
            if (listadoPoblacionesCallejero == null)
                listadoPoblacionesCallejero = new Dictionary<string, Poblacion>();

            if (!listadoPoblacionesCallejero.ContainsKey(poblacion.CodigoPoblacion))
                listadoPoblacionesCallejero.Add(poblacion.CodigoPoblacion, poblacion);
        }
        private void AñadeCodigoPostal(string codigoProvincia, string codigoMunicipio, string codigoPoblacion, CodigoPostal cp)
        {
            if (listadoCodigosPostalesCallejero == null)
                listadoCodigosPostalesCallejero = new Dictionary<string, CodigoPostal>();

            if (!listadoCodigosPostalesCallejero.ContainsKey(cp.CP))
                listadoCodigosPostalesCallejero.Add(cp.CP, cp);
        }
        private void AñadeViaACodigoPostal(string codigoProvincia, string codigoMunicipio, string codigoPoblacion, string cp, Via calle)
        {
            if (listadoViasCallejero == null)
                listadoViasCallejero = new Dictionary<string, Via>();

            if (!listadoViasCallejero.ContainsKey(calle.CodigoVia))
                listadoViasCallejero.Add(calle.CodigoVia, calle);
        }
        #endregion

        private FicheroDireccionesCallejeroDTO getNombreViaDireccionFichero(string descripcionCalle)
        {
            FicheroDireccionesCallejeroDTO direccionCallejero = new FicheroDireccionesCallejeroDTO();
            string tipoVia = string.Empty;
            string nombreVia = string.Empty;
            string complementoVia = string.Empty; //Resto de información sin el primer número.
            int primerNumeroCalle = 0;
            bool primerNumero = false;

            //Comprobamos si se ha indicado el tipo de via.
            foreach (string abr in abreviaCalles)
            {
                if(descripcionCalle.ToUpper().Contains(abr.ToUpper()))
                    tipoVia = abr;
            }

            //Separamos las descripciones por ','
            List<string> elementosCalle = descripcionCalle.Split(',').ToList();

            //Recorremos los elementos de la dirección
            for (int i = 0; i < elementosCalle.Count; i++)
            {
                //Dividimos por espacios
                elementosCalle[i].Split(' ').ForEach(subelemento =>
                {
                        if (!primerNumero && contieneNumero(subelemento))
                        {
                            int numero = recuperarNumero(subelemento);
                            string cadenaSinNumero = string.Empty;
                            cadenaSinNumero = cadenaSinNumero.Replace(numero.ToString(), "");
                            cadenaSinNumero = Regex.Replace(cadenaSinNumero, @"[^\w\s.!@$%^&*ºª()\-\/]+", "");
                            primerNumeroCalle = numero;
                            primerNumero = true;
                        }else if (primerNumero)
                        {
                            complementoVia += " " + subelemento;
                        }
                        else
                        {
                            nombreVia += " " + subelemento;
                        }
                });
            }

            direccionCallejero.calle = nombreVia.ToUpper();
            direccionCallejero.numero = primerNumeroCalle.ToString();
            direccionCallejero.complemento = complementoVia.ToUpper();
            direccionCallejero.tipoVia = tipoVia.ToUpper();

            return direccionCallejero;
        }

        //private void getCalleDireccionFichero(string descripcionCalle)
        //{
        //    string tipoVia = string.Empty;
        //    string nombreVia = string.Empty;

        //    int primerNumero = 0;
        //    int segundoNumero = 0;
        //    int tercerNumero = 0;
        //    string pisoLetras = string.Empty;
        //    string puertaletra = string.Empty;
        //    string escalera = string.Empty;
        //    bool escaleraInformada = false;

        //    //Comprobamos si se ha indicado el tipo de via.
        //    foreach(string abr in abreviaCalles)
        //    {
        //        descripcionCalle = descripcionCalle.Replace(abr, "");
        //        tipoVia = abr;
        //    }

        //    //Separamos las descripciones por ','
        //    List<string> elementosCalle = descripcionCalle.Split(',').ToList();

        //    //Recorremos los elementos de la dirección
        //    for(int i=0; i<= elementosCalle.Count; i++)
        //    {
        //        //Dividimos por espacios
        //        elementosCalle[i].Split(' ').ForEach(subelemento =>
        //        {
        //            subelemento.Split('-').ForEach(subelemento2 =>
        //            {
        //                string cadenaSinNumero = string.Empty; 
        //                if (contieneNumero(subelemento2))
        //                {
        //                    int numero = recuperarNumero(subelemento2);

        //                    if (escaleraInformada) {
        //                        escalera = numero.ToString();
        //                        escaleraInformada = false;
        //                    }
        //                    else if (primerNumero <= 0) primerNumero = numero;
        //                    else if (segundoNumero <= 0) segundoNumero = numero;
        //                    else if (tercerNumero <= 0) tercerNumero = numero;

        //                    cadenaSinNumero = cadenaSinNumero.Replace(numero.ToString(), "");
        //                    cadenaSinNumero = Regex.Replace(cadenaSinNumero, @"[^\w\s.!@$%^&*ºª()\-\/]+", "");
        //                }
        //                else
        //                {
        //                    cadenaSinNumero = subelemento2;
        //                }

        //                if(cadenaSinNumero.Length == 1)
        //                {
        //                    if (escaleraInformada)
        //                        escalera = cadenaSinNumero;
        //                    else
        //                        puertaletra = cadenaSinNumero;
        //                }
        //                else if(cadenaSinNumero.ToUpper() == "BAJO")
        //                {
        //                    pisoLetras = "Bajo";
        //                }else if (cadenaSinNumero.ToUpper() == "ESC" || cadenaSinNumero.ToUpper() == "ESC." || cadenaSinNumero.ToUpper() == "ESCALERA")
        //                {
        //                    escaleraInformada = true;
        //                }
        //                else 
        //                {
        //                    nombreVia += " " + subelemento2;
        //                }
        //            });
        //        });
        //    }
        //}

        private bool contieneNumero(string cadena)
        {
            foreach(char caracter in cadena)
            {
                int numero = -1;
                if (int.TryParse(caracter.ToString(), out numero))
                    return true;
            }
            return false;
        }

        private int recuperarNumero(string cadena)
        {
            string strNumero = string.Empty;
            int numeroReturn = -1;

            foreach(char caracter in cadena)
            {
                int numero = -1;
                if (int.TryParse(caracter.ToString(), out numero))
                    strNumero += caracter;
            }

            int.TryParse(strNumero, out numeroReturn);

            return numeroReturn;
        }

        #endregion


    }
}