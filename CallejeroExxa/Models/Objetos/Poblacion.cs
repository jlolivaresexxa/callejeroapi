﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallejeroExxa.Models.Objetos
{
    public class Poblacion
    {
        public string CodigoProvincia { get; set; }
        public string CodigoMunicipio { get; set; }
        public string CodigoPoblacion { get; set; }
        public string NombrePoblacion { get; set; }
    }
}