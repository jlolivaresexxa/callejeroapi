﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallejeroExxa.Models.Objetos
{
    public class Via
    {
        public string CodigoProvincia { get; set; }
        public string CodigoMunicipio { get; set; }
        public string CodigoPoblacion { get; set; }
        public string CodigoPostal { get; set; }
        public string CodigoVia { get; set; }
        public string NombreVia { get; set; }
        public string TipoVia { get; set; }
    }
}