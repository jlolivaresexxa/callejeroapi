﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallejeroExxa.Models.Objetos
{
    public class Municipio
    {
        public string CodigoMunicipio { get; set; }
        public string CodigoProvincia { get; set; }
        public string NombreMunicipio { get; set; }
    }
}