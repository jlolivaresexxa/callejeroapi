﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallejeroExxa.Models.Objetos
{
    public class Provincia
    {
        public string CodigoComunidad { get; set; }
        public string CodigoProvincia { get; set; }
        public string NombreProvincia { get; set; }
    }
}