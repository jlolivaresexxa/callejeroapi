﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallejeroExxa.Models.DTO
{
    [DelimitedRecord(";")]
    public class FicheroDireccionesDTO
    {
        public string id { get; set; }
        public string nombreProvincia { get; set; }
        public string nombreMunicipio { get; set; }
        public string codigoPostal { get; set; }
        public string domicilio { get; set; }
    }
}