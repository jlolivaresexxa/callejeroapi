﻿using FileHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CallejeroExxa.Models.DTO
{
    [DelimitedRecord(";")]
    public class FicheroDireccionesCallejeroDTO
    {
        public string calleCompleta { get; set; }
        public string id { get; set; }
        public string nombreProvincia { get; set; }
        public string nombreMunicipio { get; set; }
        public string codigoPostal { get; set; }
        public string tipoVia { get; set; }
        public string calle { get; set; }
        public string numero { get; set; }
        public string piso { get; set; }
        public string puerta { get; set; }
        public string complemento { get; set; }
    }
}