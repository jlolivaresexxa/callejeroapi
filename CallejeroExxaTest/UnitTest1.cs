using CallejeroGEOAPI;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CallejeroExxaTest
{
    [TestClass]
    public class PruebasAccesoGEOAPI
    {
        [TestMethod]
        public void consultaProvinciasGEOAPI()
        {
            ConsultasGEOAPI accesoGEOAPI = new ConsultasGEOAPI();
            string provinciasJSON = accesoGEOAPI.consultaProvincias();

            Assert.AreNotEqual(provinciasJSON, "");
        }
    }
}
