﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace CallejeroGEOAPI
{
    public class ConsultasGEOAPI
    {
        #region Propiedades
        private HttpClient clienteGeoAPI;
        private string key = "4b53d10dcf92ed58cfda8ccbab3c05fde291d21c059b97a9559c8d0df430e0b7";
        private string dominioAPI = "https://apiv1.geoapi.es";
        #endregion

        #region Constructor
        public ConsultasGEOAPI()
        {
            //Inicializamos HttpClient con capacidad de aceptar cookies.
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.UseCookies = true;
            clientHandler.CookieContainer = new CookieContainer();

            this.clienteGeoAPI = new HttpClient(clientHandler);
            this.clienteGeoAPI.BaseAddress = new Uri(dominioAPI);
            MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
        }
        #endregion


        #region Funciones Públicas
        /// <summary>
        /// Devuelve todas las Provincias de España
        /// </summary>
        /// <returns>JSON string</returns>
        public string consultaProvincias()
        {
            string resultJSON = string.Empty;
            try
            {
                resultJSON = this.GetProvincias();
            }catch(Exception ex)
            {
                resultJSON = "{" + string.Format("\"Error consultaProvincias \": \"{0}\"", ex.Message) + "}";
            }
            return resultJSON;
        }

        /// <summary>
        /// Devuelve todos los municipios de una provincia
        /// </summary>
        /// <returns>JSON string</returns>
        public string consultaMunicipiosPorProvincia(string codigoProvincia)
        {
            string resultJSON = string.Empty;
            try
            {
                resultJSON = this.GetMunicipiosPorProvincia(codigoProvincia);
            }
            catch (Exception ex)
            {
                resultJSON = "{" + string.Format("\"Error consultaMunicipiosPorProvincia \": \"{0}\"", ex.Message) + "}";
            }
            return resultJSON;
        }

        /// <summary>
        /// Devuelve todas las poblaciones de un municipio
        /// </summary>
        /// <returns>JSON string</returns>
        public string consultaPoblacionesPorMunicipio(string codigoProvincia, string codigoMunicipio)
        {
            string resultJSON = string.Empty;
            try
            {
                resultJSON = this.GetPoblacionesPorMunicipio(codigoProvincia, codigoMunicipio);
            }
            catch (Exception ex)
            {
                resultJSON = "{" + string.Format("\"Error consultaPoblacionesPorMunicipio \": \"{0}\"", ex.Message) + "}";
            }
            return resultJSON;
        }

        /// <summary>
        /// Devuelve todos los códigos postales de una población
        /// </summary>
        /// <returns>JSON string</returns>
        public string consultaCodigosPostalesPorPoblacion(string codigoProvincia, string codigoMunicipio, string codigoPoblacion)
        {
            string resultJSON = string.Empty;
            try
            {
                resultJSON = this.GetCodigosPostalesPorPoblacion(codigoProvincia, codigoMunicipio, codigoPoblacion);
            }
            catch (Exception ex)
            {
                resultJSON = "{" + string.Format("\"Error consultaCodigosPostalesPorPoblacion \": \"{0}\"", ex.Message) + "}";
            }
            return resultJSON;
        }

        /// <summary>
        /// Devuelve todas las calles para un código postal
        /// </summary>
        /// <returns>JSON string</returns>
        public string consultaCallejeroPostal(string codigoProvincia, string codigoMunicipio, string codigoPoblacion, string codigoPostal)
        {
            string resultJSON = string.Empty;
            try
            {
                resultJSON = this.GetCallejeroPostal(codigoProvincia, codigoMunicipio, codigoPoblacion, codigoPostal);
            }
            catch (Exception ex)
            {
                resultJSON = "{" + string.Format("\"Error consultaCallejeroPostal \": \"{0}\"", ex.Message) + "}";
            }
            return resultJSON;
        }

        /// <summary>
        /// Devuelve todas las calles coincidentes con la cadena de texto que se pasa por parámetro
        /// </summary>
        /// <returns>JSON string</returns>
        public string consultaCallejeroPorTexto(string descripcionCalle)
        {
            string resultJSON = string.Empty;
            try
            {
                resultJSON = this.GetCallesPorNombre(descripcionCalle);
            }
            catch (Exception ex)
            {
                resultJSON = "{" + string.Format("\"Error consultaCallejeroPorTexto \": \"{0}\"", ex.Message) + "}";
            }
            return resultJSON;
        }

        #endregion


        #region Métodos privados
        private string GetProvincias()
        {
            string jsonResponse = string.Empty;
            HttpWebRequest req;
            HttpWebResponse httpResponse = null;
            string urlProvincias = string.Format("{0}/{1}?key={2}", dominioAPI, "provincias", key);

            req = (HttpWebRequest)WebRequest.Create(urlProvincias);
            req.Method = "GET";
            req.ContentType = "application/json";

            httpResponse = (HttpWebResponse)req.GetResponse();
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    jsonResponse = streamReader.ReadToEnd();
                }
            }else
            {
                jsonResponse = "{\"Error \"" + httpResponse.StatusCode.ToString() + " \"" + httpResponse.StatusDescription + "\"}";
            }

            return jsonResponse;
        }

        private string GetMunicipiosPorProvincia(string codigoProvincia)
        {
            string jsonResponse = string.Empty;
            HttpWebRequest req;
            HttpWebResponse httpResponse = null;
            string urlMunicipiosProvincia = string.Format("{0}/{1}?CPRO={2}&key={3}", dominioAPI, "municipios", codigoProvincia, key);

            req = (HttpWebRequest)WebRequest.Create(urlMunicipiosProvincia);
            req.Method = "GET";
            req.ContentType = "application/json";

            httpResponse = (HttpWebResponse)req.GetResponse();
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    jsonResponse = streamReader.ReadToEnd();
                }
            }
            else
            {
                jsonResponse = "{\"Error \"" + httpResponse.StatusCode.ToString() + " \"" + httpResponse.StatusDescription + "\"}";
            }

            return jsonResponse;
        }

        private string GetPoblacionesPorMunicipio(string codigoProvincia, string codigoMunicipio)
        {
            string jsonResponse = string.Empty;
            HttpWebRequest req;
            HttpWebResponse httpResponse = null;
            string urlPoblacionesProvincia = string.Format("{0}/{1}?CPRO={2}&CMUM={3}&key={4}", dominioAPI, "poblaciones", codigoProvincia, codigoMunicipio, key);

            req = (HttpWebRequest)WebRequest.Create(urlPoblacionesProvincia);
            req.Method = "GET";
            req.ContentType = "application/json";

            httpResponse = (HttpWebResponse)req.GetResponse();
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    jsonResponse = streamReader.ReadToEnd();
                }
            }
            else
            {
                jsonResponse = "{\"Error \"" + httpResponse.StatusCode.ToString() + " \"" + httpResponse.StatusDescription + "\"}";
            }

            return jsonResponse;
        }

        private string GetCodigosPostalesPorPoblacion(string codigoProvincia, string codigoMunicipio, string codigoPoblacion)
        {
            string jsonResponse = string.Empty;
            HttpWebRequest req;
            HttpWebResponse httpResponse = null;
            string urlPoblacionesProvincia = string.Format("{0}/{1}?CPRO={2}&CMUM={3}&CUN={4}&key={5}", dominioAPI, "cps", codigoProvincia, codigoMunicipio, codigoPoblacion, key);

            req = (HttpWebRequest)WebRequest.Create(urlPoblacionesProvincia);
            req.Method = "GET";
            req.ContentType = "application/json";

            httpResponse = (HttpWebResponse)req.GetResponse();
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    jsonResponse = streamReader.ReadToEnd();
                }
            }
            else
            {
                jsonResponse = "{\"Error \"" + httpResponse.StatusCode.ToString() + " \"" + httpResponse.StatusDescription + "\"}";
            }

            return jsonResponse;
        }

        private string GetCallejeroPostal(string codigoProvincia, string codigoMunicipio, string codigoPoblacion, string codigoPostal)
        {
            string jsonResponse = string.Empty;
            HttpWebRequest req;
            HttpWebResponse httpResponse = null;
            string urlPoblacionesProvincia = string.Format("{0}/{1}?CPRO={2}&CMUM={3}&CUN={4}&CPOS={5}&key={6}", dominioAPI, "calles", codigoProvincia, codigoMunicipio, codigoPoblacion, codigoPostal, key);

            req = (HttpWebRequest)WebRequest.Create(urlPoblacionesProvincia);
            req.Method = "GET";
            req.ContentType = "application/json";

            httpResponse = (HttpWebResponse)req.GetResponse();
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    jsonResponse = streamReader.ReadToEnd();
                }
            }
            else
            {
                jsonResponse = "{\"Error \"" + httpResponse.StatusCode.ToString() + " \"" + httpResponse.StatusDescription + "\"}";
            }

            return jsonResponse;
        }

        private string GetCallesPorNombre(string nombreCalle)
        {
            string jsonResponse = string.Empty;
            HttpWebRequest req;
            HttpWebResponse httpResponse = null;
            string urlProvincias = string.Format("{0}/{1}?QUERY={2}&key={3}", dominioAPI, "qcalles", nombreCalle, key);

            req = (HttpWebRequest)WebRequest.Create(urlProvincias);
            req.Method = "GET";
            req.ContentType = "application/json";

            httpResponse = (HttpWebResponse)req.GetResponse();
            if (httpResponse.StatusCode == HttpStatusCode.OK)
            {
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    jsonResponse = streamReader.ReadToEnd();
                }
            }
            else
            {
                jsonResponse = "{\"Error \"" + httpResponse.StatusCode.ToString() + " \"" + httpResponse.StatusDescription + "\"}";
            }

            return jsonResponse;
        }
        #endregion
    }
}
