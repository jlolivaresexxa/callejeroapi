﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CallejeroGEOAPI.ObjetosAPI
{
    public class CalleGEOAPI
    {
        public string CPRO { get; set; }
        public string CMUM { get; set; }
        public string CPOS { get; set; }
        public string CUN { get; set; }
        public string CVIA { get; set;}
        public string NVIAC { get; set; }
        public string TVIA { get; set; }
    }
}
