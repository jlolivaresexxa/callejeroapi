﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CallejeroGEOAPI.ObjetosAPI
{
    public class PoblacionGEOAPI
    {
        public string CMUM { get; set; }
        public string CPRO { get; set; }
        public string CUN { get; set; }
        public string NENTSI50 { get; set; }
    }
}
