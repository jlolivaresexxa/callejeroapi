﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CallejeroGEOAPI.ObjetosAPI
{
    public class ProvinciaGEOAPI
    {
        public string CCOM { get; set; }
        public string CPRO { get; set; }
        public string PRO { get; set; }
    }
}
