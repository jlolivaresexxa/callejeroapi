﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CallejeroGEOAPI.ObjetosAPI
{
    public class CodigoPostalGEOAPI
    {
        public string CPRO { get; set; }
        public string CMUM { get; set; }
        public string CUN { get; set; }
        public string CPOS { get; set; }
    }
}
