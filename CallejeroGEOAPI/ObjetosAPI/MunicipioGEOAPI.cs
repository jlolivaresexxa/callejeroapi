﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CallejeroGEOAPI.ObjetosAPI
{
    public class MunicipioGEOAPI
    {
        public string CMUM { get; set; }
        public string CPRO { get; set; }
        public string DMUN50 { get; set; }
    }
}
